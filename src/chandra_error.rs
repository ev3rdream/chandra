/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use cryex::CryexError;

use crate::{api_error::ApiError, config_error::ConfigError};

pub(crate) type Result<T> = std::result::Result<T, ChandraError>;

pub(crate) enum ChandraError {
    MissingConfiguration,
    ConfigurationError(ConfigError),
    ApiError(ApiError),
    CryexError(CryexError),
    ChannelFull,
}

impl std::fmt::Display for ChandraError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ChandraError::MissingConfiguration => write!(f, "Missing configuration file!"),
            ChandraError::ConfigurationError(err) => write!(f, "Configuration error - {}", err),
            ChandraError::ApiError(err) => write!(f, "Api error - {}", err),
            ChandraError::CryexError(err) => write!(f, "exchange error - {}", err),
            ChandraError::ChannelFull => write!(f, "Send channel is full!"),
        }
    }
}

// configuration error conversion
impl std::convert::From<ConfigError> for ChandraError {
    fn from(config_error: ConfigError) -> Self {
        ChandraError::ConfigurationError(config_error)
    }
}

// cryex error conversion
impl std::convert::From<CryexError> for ChandraError {
    fn from(cryex_error: CryexError) -> Self {
        ChandraError::CryexError(cryex_error)
    }
}

impl<T> std::convert::From<async_std::channel::SendError<T>> for ChandraError {
    fn from(_: async_std::channel::SendError<T>) -> Self {
        ChandraError::ChannelFull
    }
}
