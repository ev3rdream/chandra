/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use std::num::TryFromIntError;

pub(crate) type Result<T> = std::result::Result<T, ConfigError>;

#[derive(Debug)]
pub(crate) enum ConfigError {
    FileSystemError(std::io::Error),
    JsonError(serde_json::Error),
    WrongApplicationName(String),
    WrongVersion(String),
    IntParsingError(TryFromIntError),
}

impl std::fmt::Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ConfigError::WrongApplicationName(name) => {
                write!(f, "Wrong application name: {}", name)
            }
            ConfigError::WrongVersion(version) => {
                write!(f, "Wrong application version: {}", version)
            }
            ConfigError::IntParsingError(err) => write!(f, "Number conversion error: {}", err),
            ConfigError::JsonError(err) => write!(f, "json error: {}", err),
            ConfigError::FileSystemError(err) => write!(f, "file system error: {}", err),
        }
    }
}

// conversion error conversion
impl std::convert::From<TryFromIntError> for ConfigError {
    fn from(from_int_error: TryFromIntError) -> Self {
        ConfigError::IntParsingError(from_int_error)
    }
}

impl std::convert::From<serde_json::Error> for ConfigError {
    fn from(json_error: serde_json::Error) -> Self {
        ConfigError::JsonError(json_error)
    }
}

impl std::convert::From<std::io::Error> for ConfigError {
    fn from(file_error: std::io::Error) -> Self {
        ConfigError::FileSystemError(file_error)
    }
}
