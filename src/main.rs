/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use async_std::task;
use clap::{crate_version, Arg, Command};

use crate::chandra_error::{ChandraError, Result};

mod api;
mod api_error;
mod chandra;
mod chandra_error;
mod config;
mod config_error;

fn main() {
    let config = match parse_args() {
        Ok(config) => config,
        Err(err) => {
            eprintln!("{}", err);
            return;
        }
    };
    if let Err(err) = task::block_on(chandra::run(config)) {
        eprintln!("{}", err);
    }
}

// parse arguments using clap
// Chandra takes one mandatory argument, path to a config file
fn parse_args() -> Result<String> {
    Command::new("Runner")
        .version(crate_version!())
        .author("Jurij Robba <jurij.robba@vernocte.org>")
        .about("Unified data retrieving and connection to exchanges")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true)
                .required(true),
        )
        .get_matches()
        .value_of("config")
        .ok_or(ChandraError::MissingConfiguration)
        .map(|val| val.to_owned())
}
