/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use async_std::channel::Sender;
use async_tungstenite::WebSocketStream;
use clap::crate_version;
use cryex::{Command, Data, Exchange, SupportedExchange};
use serde_json::json;
use serde_json::Value;
use std::{collections::HashMap, net::SocketAddr, sync::Arc};

use futures::SinkExt;

use async_std::{
    channel::Receiver,
    net::{TcpListener, TcpStream},
    sync::RwLock,
    task,
};
use async_tungstenite::tungstenite::protocol::Message;
use futures::stream::StreamExt;

use crate::api_error::ApiError;
use crate::api_error::Result;

type PeerMap = Arc<
    RwLock<HashMap<SocketAddr, futures::stream::SplitSink<WebSocketStream<TcpStream>, Message>>>,
>;

pub(crate) async fn run(exchange: Exchange, port: u16) -> Result<()> {
    let address = format!("127.0.0.1:{}", port);

    // Create the event loop and TCP listener we'll accept connections on.
    let listener = TcpListener::bind(&address).await?;
    println!("Websocket server established on: {}", address);

    let state = PeerMap::new(RwLock::new(HashMap::new()));
    task::spawn(broadcast_data(exchange.data_rx, state.clone()));

    println!("waiting for connections!");

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, client)) = listener.accept().await {
        task::spawn(handle_connection(
            state.clone(),
            stream,
            client,
            exchange.command_tx.clone(),
            exchange.exchange_type.clone(),
        ));
    }
    Ok(())
}

async fn handle_connection(
    peer_map: PeerMap,
    raw_stream: TcpStream,
    address: SocketAddr,
    tx: Sender<Command>,
    exchange: SupportedExchange,
) -> Result<()> {
    let ws_stream = match async_tungstenite::accept_async(raw_stream).await {
        Ok(stream) => stream,
        Err(err) => {
            println!("Socket error: {:?}", &err);
            return Err(ApiError::WebsocketError(err));
        }
    };
    println!("WebSocket connection established: {}", address);

    // Insert the write part of this peer to the peer map.
    let (tx_socket, mut rx_socket) = ws_stream.split();
    peer_map.write().await.insert(address, tx_socket);

    let intro = json!(
        {
            "application": "chandra",
            "version": crate_version!(),
            "exchange": format!("{}", exchange),
        }
    );
    peer_map
        .write()
        .await
        .get_mut(&address)
        .ok_or(ApiError::MisconfiguredPeerList)?
        .send(Message::Text(intro.to_string()))
        .await?;
    while let Some(message) = rx_socket.next().await {
        let message = message?;
        match message {
            Message::Text(message) => match get_command(message) {
                Ok(cmd) => tx.try_send(cmd)?,
                Err(err) => eprintln!("Got invalid message from client: {}", err),
            },
            Message::Ping(data) => {
                peer_map
                    .write()
                    .await
                    .get_mut(&address)
                    .ok_or(ApiError::MisconfiguredPeerList)?
                    .send(Message::Pong(data))
                    .await?;
            }
            Message::Close(_) => todo!(),
            _ => todo!(),
        }
    }

    println!("{} disconnected", &address);
    peer_map.write().await.remove(&address);

    Ok(())
}

async fn broadcast_data(mut rx: Receiver<Data>, peers: PeerMap) -> Result<()> {
    loop {
        let data = match rx.next().await {
            Some(data) => data,
            None => return Ok(()), // stream closed
        };
        let message = get_message(data);
        let mut list = peers.write().await;
        for (_, tx) in list.iter_mut() {
            tx.send(message.clone()).await?;
        }
    }
}

fn get_message(_data: Data) -> Message {
    Message::Text(String::from(""))
}

fn get_command(message: String) -> Result<Command> {
    let msg: Value = serde_json::from_str(&message)?;
    let command = msg.get("command").and_then(|val| val.as_str());
    if let Some(command) = command {
        return match command {
            "get currencies" => Ok(Command::GetCurrencies),
            "get pairs" => Ok(Command::GetPairs),
            _ => Err(ApiError::UnknownMessage(message)),
        };
    }

    Err(ApiError::UnknownMessage(message))
}
