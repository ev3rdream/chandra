/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use crate::chandra_error::ChandraError;

use crate::{chandra_error::Result, config::Config};

pub(crate) async fn run(config: String) -> Result<()> {
    // parse config file
    let config = Config::create(config)?;

    // create exchange
    let exchange = cryex::Exchange::create(config.exchange)?;

    exchange.command_tx.send(cryex::Command::GetPairs).await?;

    crate::api::run(exchange, config.port)
        .await
        .map_err(ChandraError::ApiError)
}
