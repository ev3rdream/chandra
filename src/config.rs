/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

use cryex::SupportedExchange;

use crate::config_error::{ConfigError, Result};
use clap::crate_version;
use serde::Deserialize;

fn default_port() -> u16 {
    1808u16
}

fn default_exchange() -> SupportedExchange {
    SupportedExchange::HitBTC
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct Config {
    pub(crate) application: String,
    pub(crate) version: String,
    #[serde(default = "default_exchange")]
    pub(crate) exchange: SupportedExchange,
    #[serde(default = "default_port")]
    pub(crate) port: u16,
}

impl Config {
    pub(crate) fn create(path: String) -> Result<Config> {
        let file = std::fs::File::open(&path)?;
        let config: Config = serde_json::from_reader(file)?;
        Config::verify_config(&config)?;
        Ok(config)
    }

    // verifies that config fits application name and version
    fn verify_config(config: &Config) -> Result<()> {
        if config.application != "chandra" {
            return Err(ConfigError::WrongApplicationName(
                config.application.to_string(),
            ));
        }
        if config.version != crate_version!() {
            return Err(ConfigError::WrongVersion(config.version.to_string()));
        }
        Ok(())
    }
}
