/*============================================================================*\

                          Copyright Jurij Robba 2022
                              All rights reserved

\*============================================================================*/

pub(crate) type Result<T> = std::result::Result<T, ApiError>;

#[derive(Debug)]
pub(crate) enum ApiError {
    IOError(std::io::Error),
    WebsocketError(async_tungstenite::tungstenite::Error),
    MisconfiguredPeerList,
    ChannelError,
    InvalidMessage(serde_json::Error),
    UnknownMessage(String),
}

impl std::fmt::Display for ApiError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ApiError::IOError(err) => write!(f, "Input/output error: {}", err),
            ApiError::WebsocketError(err) => write!(f, "Websocket error: {}", err),
            ApiError::MisconfiguredPeerList => write!(f, "Misconfigured client list"),
            ApiError::ChannelError => write!(f, "Unexpected Channel error"),
            ApiError::InvalidMessage(err) => write!(f, "Invalid json received: {}", err),
            ApiError::UnknownMessage(msg) => write!(f, "Unknown message: {}", msg),
        }
    }
}

// conversion error conversion
impl std::convert::From<std::io::Error> for ApiError {
    fn from(io_error: std::io::Error) -> Self {
        ApiError::IOError(io_error)
    }
}

// conversion error conversion
impl std::convert::From<async_tungstenite::tungstenite::Error> for ApiError {
    fn from(websocket_error: async_tungstenite::tungstenite::Error) -> Self {
        ApiError::WebsocketError(websocket_error)
    }
}

// send error conversion
impl<T> std::convert::From<async_std::channel::TrySendError<T>> for ApiError {
    fn from(_send_error: async_std::channel::TrySendError<T>) -> Self {
        ApiError::ChannelError
    }
}

// send error conversion
impl std::convert::From<serde_json::Error> for ApiError {
    fn from(json_error: serde_json::Error) -> Self {
        ApiError::InvalidMessage(json_error)
    }
}
